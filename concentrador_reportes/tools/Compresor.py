import os
import zipfile

class Compresor():
    def __init__(self,folder,out_zip='./out.zip'):
        self.folder = folder
        self.out_zip = out_zip
        self.zip_pack = zipfile.ZipFile(self.out_zip,'w')

    def comprimir_archivos(self):
        for folder, subfolder, files in os.walk(self.folder):
            for file in files:
                if file.endswith('.pdf') or file.endswith('.xlsx'):
                    self.zip_pack.write(os.path.join(folder, file), os.path.relpath(os.path.join(folder,file), self.folder), compress_type = zipfile.ZIP_DEFLATED)
        self.zip_pack.close()