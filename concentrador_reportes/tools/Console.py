import argparse
from concentrador_reportes.tools.Server import Server
class Console(object):
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('--host','-H',nargs='?',type=str,default='localhost',help='recibe el host con el cual estara escuchando el servidor, 0.0.0.0 para todas las ip')
        self.parser.add_argument('--port','-P',nargs='?',type=int,default=3000,help='Recibe el puerto en el cual estara escuchado el servidor')
        self.parser.add_argument('--debug',default=False, action="store_true", help='modo debug')
        self.args = self.parser.parse_args()
    
    def iniciar_consola(self):
        server = Server("concentrador_reportes")
        server.app.run(host=self.args.host,port=self.args.port,debug=self.args.debug)
