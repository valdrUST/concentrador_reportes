from reportlab.lib.pagesizes import letter
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.utils import ImageReader

class PDF_report():
    def __init__(self,out_file="./out.pdf"):
        self.out_file = out_file
        self.canvas = Canvas(self.out_file, pagesize=letter)
    
    def add_image(self,img_file,x,y,page=False):
        try:
            if(page == True):
                self.canvas.showPage()
            image = ImageReader(img_file)
            self.canvas.drawImage(image,x,y,mask='auto',width=400,height=300,preserveAspectRatio=True)
        except Exception as e:
            print(e)
    
    def add_text(self,text,x,y,page=False):
        if(page == True):
            self.canvas.showPage()
        self.canvas.drawString(x,y,text)
        

    def create_pdf(self):
        self.canvas.save()
        return self.canvas.getpdfdata()

