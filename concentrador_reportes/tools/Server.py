from flask import Flask, escape, request, Blueprint, jsonify, make_response
from . import Graficador
from . import PDF_report
from . import Compresor
from . import Mail
from os import mkdir, path
import base64

server_report = Blueprint('app', __name__, url_prefix='/')
class Server():
    def __init__(self, name, *args, **kwargs):
        self.app = Flask(name)
        self.app.register_blueprint(server_report)
        if not path.exists("../server_file"):
            mkdir("../server_file")

@server_report.route('/')
def index():
    return "<h1>Hello workd</h1>"

@server_report.route('/create_report',methods=['POST'])
def create_report():
    datos = request.get_json()
    resumenPlanesGeneracion = datos['resumenPlanesGeneracion']
    planesEstudio = datos['planesEstudio']
    data = {
        "labels":["lb1","lb2","lb3"],
        "varlabels":["var1","var2","var3","var4"],
            "data":[
                [1,2,3,2],
                [1,1,3,2],
                [1,2,1,2]
                ]
            }
    chart_file = "../server_file/grafica.png"
    g = Graficador(data,out_file=chart_file)
    g.crear_grafica_radar()
    pdf = PDF_report(out_file="../server_file/reporte.pdf")
    pdf.add_image(chart_file,0,400)
    pdf.create_pdf()
    compresor = Compresor("../server_file/","../server_file/reporte.zip")
    compresor.comprimir_archivos()
    try:
        mail = Mail("../server_file/reporte.zip","valdr.stiglitz@gmail.com")
    except Exception as e:
        print(e)
        return "Error"
    mail.create_mail()
    return mail.send_mail()

# esta peticion es la que retorna los datos del pdf para la descarga con php
@server_report.route('/create_report_pdf',methods=['POST'])
def create_report_pdf():
    datos = request.get_json()
    resumenPlanesGeneracion = datos['resumenPlanesGeneracion']
    planesEstudio = datos['planesEstudio']
    data = {
        "labels":["lb1","lb2","lb3"],
        "varlabels":["var1","var2","var3","var4"],
        "data":[
            [1,2,3,2],
            [1,1,3,2],
            [1,2,1,2]
            ]
        }
    chart_file_1 = "../server_file/grafica.png"
    chart_file_2 = "../server_file/grafica2.png"
    g = Graficador(data,out_file=chart_file_1)
    g.crear_grafica_radar()
    data = {
        "title":"grafica lineal",
        "data":[
            [1,2,3,4,5,6,1],
            [1,4,3,4,2,3,6]
            ],
        "labels":["datos1","datos2"],
        "xlabel":"range x",
        "ylabel":"range y"
        }
    g = Graficador(data,out_file=chart_file_2)
    g.crear_grafica_lineal()
    pdf = PDF_report(out_file="../server_file/reporte.pdf")
    pdf.add_image(chart_file_1,50,400)
    pdf.add_image(chart_file_2,50,50)
    pdf.add_text("hola soy un texto",0,600,True)
    pdf_file = pdf.create_pdf()
    return pdf_file