import numpy as np 

import matplotlib.pyplot as plt
from math import pi


class Graficador():
    def __init__(self, data, out_file=None, type = None):
        self.data = data
        self.out_file = out_file
        self.type = type

    def crear_grafica_radar(self):
        N = len(self.data["varlabels"])
        Attributes = self.data["varlabels"]
        angles = [n / N * 2 * pi for n in range(N)]
        angles += angles [:1]
        label_num = 0
        label_pos = .9
        ax = plt.subplot(111,polar=True)
        for data_list in self.data["data"]:
            values = data_list
            values += values[:1]
            plt.xticks(angles[:-1],Attributes)
            ax.plot(angles,values)
            ax.plot(angles, values)
            ax.fill(angles, values, 'teal', alpha=0.1)
            label_pos += .5
            label_num += 1
        plt.legend(self.data["labels"], loc=(0.9, .95),labelspacing=0.1, fontsize='small')
        plt.savefig(self.out_file)
        plt.close()
    
    def crear_grafica_lineal(self):
        ax = plt.subplot()
        ax.set_title(self.data["title"])
        ax.set_xlabel(self.data["xlabel"])
        ax.set_ylabel(self.data["ylabel"])
        for data_list in self.data["data"]:
            ax.plot(data_list)
        plt.legend(self.data["labels"],labelspacing=0.1, fontsize='small')
        plt.savefig(self.out_file)
        plt.close()





if __name__ == "__main__":
    data = {"title":"grafica",
            "labels":["lb1","lb2","lb3"],
            "varlabels":["var1","var2","var3","var4"],
            "data":[[1,2,3,2],[1,1,3,2],[1,2,1,2]]
            }

    g = Graficador(data)
    g.crear_grafica_radar()