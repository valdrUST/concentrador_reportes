# Concentrador
## Instalacion
```
$ pip install .
```
## uso 
* Modo desarrollo
```
$ concentrador_reportes [-h] [--host [HOST]] [--port [PORT]] [--debug]

optional arguments:
  -h, --help            show this help message and exit
  --host [HOST], -H [HOST]
                        recibe el host con el cual estara escuchando el servidor, 0.0.0.0 para
                        todas las ip
  --port [PORT], -P [PORT]
                        Recibe el puerto en el cual estara escuchado el servidor
  --debug               modo debug
```

* Modo produccion
```
$ concentrador_reportesWSGI
```
